package ac.rs.uns.ftn.projekat.repository;


import ac.rs.uns.ftn.projekat.entity.Dostavljac;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

@Repository
public interface DostavljacRep extends JpaRepository<Dostavljac, Long> {
}