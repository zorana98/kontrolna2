
package ac.rs.uns.ftn.projekat.repository;

import ac.rs.uns.ftn.projekat.entity.Artikal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ArtikalRep extends JpaRepository<Artikal,Long> {
    List<Artikal> findAll();
    List<Artikal> findByNaziv(String naziv);
    List<Artikal> findByKategorija(String kategorija);
 

   
    
}