package ac.rs.uns.ftn.projekat.repository;

import ac.rs.uns.ftn.projekat.entity.Artikal;
import ac.rs.uns.ftn.projekat.entity.Korpa;
import ac.rs.uns.ftn.projekat.entity.Status;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface KorpaRep extends JpaRepository<Korpa,Long> {

	List<Korpa> findAllByStatus(Status status);

	Korpa findByIdAndStatus(Long idUsera, Status kupljeno);

}

