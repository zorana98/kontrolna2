
package ac.rs.uns.ftn.projekat.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class Korisnik implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="korisnickoime")
    private String korisnickoime;
    @Column(name="popust")
    private int popust;
    @Column(name="lozinka")
    private String lozinka;
    @Column(name="ime")
    private String ime;
    @Column(name="prezime")
    private String prezime;
    @Column(name="uloga")
    private Uloga uloga;
    @Column(name="telefon")
    private String telefon;
    @Column(name="email")
    private String email;
    @Column(name="adresa")
    private String adresa;

    @Override
    public String toString() {
        return "Kupac{" +
                "korisnickoIme='" + korisnickoime + '\'' +
                ", ime='" + ime + '\'' +
                ", prezime='" + prezime + '\'' +
                ", lozinka='" + lozinka + '\'' +
                ", uloga='" + uloga + '\'' +
                ", telefon=" + telefon +
                ", mail='" + email + '\'' +
                ", adresa='" + adresa + '\'' +
                '}';
    }
    
    public void setPopust(int popust) {
        this.popust = popust;
    }

    public int getPopust() {
        return popust;
    }

    public void setKorisnickoime(String korisnickoIme) {
        this.korisnickoime = korisnickoIme;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public void setUloga(Uloga uloga) {
        this.uloga = uloga;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public void setMail(String email) {
        this.email = email;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getKorisnickoime() {
        return korisnickoime;
    }
    public Long getId(){
        return  id;
    }
    public String getIme() {
        return ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public String getLozinka() {
        return lozinka;
    }

    public Uloga getUloga() {
        return uloga;
    }

    public String getTelefon() {
        return telefon;
    }

    public String getEmail() {
        return email;
    }

    public String getAdresa() {
        return adresa;
    }
    
    @ManyToMany
    @JoinTable(name = "omiljeni", joinColumns = @JoinColumn(name = "korisnik_id",referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "artikal_id",referencedColumnName = "id"))
    private List<Artikal> omiljeniArtikli = new ArrayList<Artikal>();
   

	public List<Artikal> getOmiljeniArtikli() {
        return omiljeniArtikli;
    }

    public void setOmiljeniArtikli(List<Artikal> omiljeni) {
        this.omiljeniArtikli = omiljeni;
    }
    public void setOmiljeniArtikli(Artikal artikal) {
        this.omiljeniArtikli.add(artikal);
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "korpa_id")
    private Korpa korpaKupac;

    public Korpa getKorpa() {
        return korpaKupac;
    }

    public void setKorpa(Korpa korpa) {
        this.korpaKupac = korpa;
    }

	public List<Korpa> getKupljeneKorpa() {
        return kupljeneKorpe;
    }
    public void setKupljeneKorpa(Korpa korpa) {
        this.kupljeneKorpe.add(korpa);
    }
    @ManyToMany
    @JoinTable(name = "kupljeneKorpe", joinColumns = @JoinColumn(name = "korisnik_id",referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "korpa_id",referencedColumnName = "id"))
    private List<Korpa> kupljeneKorpe = new ArrayList<Korpa>();
}

