package ac.rs.uns.ftn.projekat.controller;

import ac.rs.uns.ftn.projekat.entity.Korisnik;
import ac.rs.uns.ftn.projekat.entity.Korpa;
import ac.rs.uns.ftn.projekat.entity.Uloga;
import ac.rs.uns.ftn.projekat.repository.KorisnikRep;
//import ac.rs.uns.ftn.projekat.entity.Uloga;
import ac.rs.uns.ftn.projekat.service.KorisnikService;
//import ch.qos.logback.core.joran.conditional.ElseAction;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

@Controller
public class KorisnikController {
    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private KorisnikRep korisnikRep;
    @GetMapping("/")
    public String welcome() {
        return "home.html";
    }

    @GetMapping("/registracija")
    public String registrovanje(Model model) {
        Korisnik korisnik = new Korisnik();
        model.addAttribute("korisnik", korisnik);
        return "registracija.html";
    }

    @PostMapping(value = "/registracija")
    public String addUser(@Valid @ModelAttribute Korisnik korisnik, BindingResult errors, Model model)
            throws Exception {
        List<Korisnik> bk = korisnikService.findName(korisnik.getKorisnickoime());
        if (!bk.isEmpty()) {
            model.addAttribute("poruka_reg", "Vec postoji username");
            return "neuspesnaregistracija.html";
        } else {
            model.addAttribute("poruka_reg", null);
            this.korisnikService.create(korisnik);
            model.addAttribute("kupac", this.korisnikService.findAll());
            Uloga uloga = korisnik.getUloga();
            model.addAttribute("uloga", uloga.toString());
            System.out.println(uloga.toString());
            /*if (uloga.toString().equalsIgnoreCase("Kupac")) {
                return "homekupac.html";
            }
            if (uloga.toString().equalsIgnoreCase("Administrator")) {
                return "homeadmin.html";
            }
            if (uloga.toString().equalsIgnoreCase("Dostavljac")) {
                return "homedostavljac.html";
            }*/
            /*
             * Uloga uloga =
             * this.korisnikService.findAll().get(korisnici.size()-1).getUloga(); /*values =
             * uloga.values(); model.addAttribute("uloga",
             * uloga.values()[korisnici.size()-1].toString());
             * 
             * System.out.println(uloga.values()[korisnici.size()-1].toString());
             */

            return "home.html";
        }

    }

    public HashMap<String, String> generateKorisnikDao(Korisnik korisnik) {

        HashMap<String, String> korisnikDao = new HashMap<String, String>();
        korisnikDao.put("uloga", korisnik.getUloga().toString());
        korisnikDao.put("id", korisnik.getId().toString());
        korisnikDao.put("ime", korisnik.getIme());
        korisnikDao.put("prezime", korisnik.getPrezime());
        korisnikDao.put("korisnickoIme", korisnik.getKorisnickoime());
        korisnikDao.put("lozinka", korisnik.getLozinka());
        korisnikDao.put("adresa", korisnik.getAdresa());
        korisnikDao.put("telefon", korisnik.getTelefon());
        korisnikDao.put("email", korisnik.getEmail());
        return korisnikDao;
    }
    public Long generateId(Korisnik korisnik) {

    
        return korisnik.getId();
    }
    public String generateUloga(Korisnik korisnik) {

    
        return korisnik.getUloga().toString();
    }

    @PostMapping("/login")
    public String getLoddedKorsinka(@Valid @ModelAttribute Korisnik kupac, BindingResult errors, Model model)
            throws Exception {
        try {
            Korisnik korisnik = korisnikService.findNamePassword(kupac.getKorisnickoime(), kupac.getLozinka());

            HashMap<String, String> korisnikDao = generateKorisnikDao(korisnik);
            Long korisnikId=generateId(korisnik);
            String korisnikString = korisnikDao.toString();
            Uloga uloga = korisnik.getUloga();
            // model.addAttribute("uloga", uloga.toString());
            model.addAttribute("user", korisnikDao);
            model.addAttribute("id", korisnikId);
            if (uloga.toString().equalsIgnoreCase("Kupac")) {
                return "homekupac.html";
                
                
                
            }
            if (uloga.toString().equalsIgnoreCase("Administrator")) {
                return "homeadmin.html";
            }
            if (uloga.toString().equalsIgnoreCase("Dostavljac")) {
                return "homedostavljac.html";
            }
            return "redirect:/home";
        } catch (Exception e) {
            model.addAttribute("poruka_log", "Pokusaj ponovo");
            return "neuspesno.html";
        }
    }

    @GetMapping("/login")
    public String getLogin(Model model) {
        return "login.html";
    }

    @GetMapping("/korisnik")
    public String korisnik(Model model) {
        List<Korisnik> korisnici = this.korisnikService.findAll();
        model.addAttribute("korisnici", korisnici);
        return "korisnici.html";
    }

    @GetMapping("/korisnici/{id}")
    public String getKorisnik(@PathVariable(name = "id") Long id, Model model) {
        Korisnik korisnik = this.korisnikService.findOne(id);
        model.addAttribute("korisnik", korisnik);
        return "korisnik.html";
    }

    @GetMapping("/change/{id}")
    public String changeUser(@PathVariable("id") long id, Model model) {
        
        Korisnik korisnik=this.korisnikService.findOne(id);
        if(korisnik.getUloga().toString().equals("ADMINISTRATOR")){
            korisnik.setUloga(Uloga.KUPAC);
        }else
        {korisnik.setUloga(Uloga.ADMINISTRATOR); }
        this.korisnikRep.save(korisnik);
        model.addAttribute("korisnici", korisnikService.findAll());
        return "korisnici.html";
    }
    /*@GetMapping("/izmenjeno")
    public String getOmiljeno(Model model) {

        List<Korisnik> korisnici = this.korisnikService.findAll();
        model.addAttribute("korisnici", korisnici);
        return "korisnici.html";

    }*/
    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") long id, Model model) {
        this.korisnikService.delete(id);
        model.addAttribute("kupci", korisnikService.findAll());
        return "redirect:/korisnik";
    }
    @GetMapping("/izmeni/{id}/{ime}")
    public String changeIme(@PathVariable("id") long id,@PathVariable("ime") String ime,Model model) {

        Korisnik korisnik=this.korisnikService.findOne(id);
        korisnik.setIme(ime);
        this.korisnikRep.save(korisnik);
        model.addAttribute("korisnici", korisnikService.findAll());
        return "korisnici.html";
    }
    @GetMapping("/izmeni/{id}/1/{prezime}")
    public String changePrezime(@PathVariable("id") long id,@PathVariable("prezime") String prezime,Model model) {

        Korisnik korisnik=this.korisnikService.findOne(id);
        korisnik.setPrezime(prezime);
        this.korisnikRep.save(korisnik);
        model.addAttribute("korisnici", korisnikService.findAll());
        return "korisnici.html";
    }
    @GetMapping("/izmeni/{id}/1/1/{email}")
    public String changeEmail(@PathVariable("id") long id,@PathVariable("email") String email,Model model) {

        Korisnik korisnik=this.korisnikService.findOne(id);
        korisnik.setMail(email);
        this.korisnikRep.save(korisnik);
        model.addAttribute("korisnici", korisnikService.findAll());
        return "korisnici.html";
    }
    @GetMapping("/izmeni/{id}/1/1/1/{korisnickoime}")
    public String changekorisnicko(@PathVariable("id") long id,@PathVariable("korisnickoime") String korisnickoime,Model model) {

        Korisnik korisnik=this.korisnikService.findOne(id);
        korisnik.setKorisnickoime(korisnickoime);
        this.korisnikRep.save(korisnik);
        model.addAttribute("korisnici", korisnikService.findAll());
        return "korisnici.html";
    }
    @GetMapping("/izmeni/{id}/1/1/1/1/{adresa}")
    public String changeadresa(@PathVariable("id") long id,@PathVariable("adresa") String adresa,Model model) {

        Korisnik korisnik=this.korisnikService.findOne(id);
        korisnik.setAdresa(adresa);
        this.korisnikRep.save(korisnik);
        model.addAttribute("korisnici", korisnikService.findAll());
        return "korisnici.html";
    }
    @GetMapping("/izmeni/{id}/1/1/1/1/1/{telefon}")
    public String changetelefon(@PathVariable("id") long id,@PathVariable("telefon") String telefon,Model model) {

        Korisnik korisnik=this.korisnikService.findOne(id);
        korisnik.setTelefon(telefon);
        this.korisnikRep.save(korisnik);
        model.addAttribute("korisnici", korisnikService.findAll());
        return "korisnici.html";
    }
    @GetMapping("/izmeni/{id}/1/1/1/1/1/1/{uloga}")
    public String changeUloga(@PathVariable("id") long id,@PathVariable("uloga") String uloga,Model model) {

        Korisnik korisnik=this.korisnikService.findOne(id);
        if(uloga.equalsIgnoreCase("administrator")){korisnik.setUloga(Uloga.ADMINISTRATOR);}
        else{if(uloga.equalsIgnoreCase("kupac")){korisnik.setUloga(Uloga.KUPAC);}
        else{korisnik.setUloga(Uloga.DOSTAVLJAC);}
        }
        this.korisnikRep.save(korisnik);
        model.addAttribute("korisnici", korisnikService.findAll());
        return "korisnici.html";
    }
    @GetMapping("/izmeni/{ime}/{prezime}/{mail}/{korisnicko}/{telefon}/{adresa}/{uloga}/{lozinka}")
    public String dodajKorisnika(@PathVariable("ime") String ime,@PathVariable("prezime") String prezime,@PathVariable("mail") String mail,@PathVariable("korisnicko") String korisnicko,@PathVariable("adresa") String adresa,@PathVariable("uloga") String uloga,@PathVariable("telefon") String telefon,@PathVariable("lozinka") String lozinka,Model model) {

        Korisnik korisnik=new Korisnik();
        korisnik.setIme(ime);
        korisnik.setPrezime(prezime);
        korisnik.setAdresa(adresa);
        korisnik.setKorisnickoime(korisnicko);
        korisnik.setMail(mail);
        korisnik.setTelefon(telefon);
        if(uloga.equalsIgnoreCase("administrator")){korisnik.setUloga(Uloga.ADMINISTRATOR);}
        else{if(uloga.equalsIgnoreCase("kupac")){korisnik.setUloga(Uloga.KUPAC);}
        else{korisnik.setUloga(Uloga.DOSTAVLJAC);}
        }
        korisnik.setLozinka(lozinka);
        this.korisnikRep.save(korisnik);
        model.addAttribute("korisnici", korisnikService.findAll());
        return "korisnici.html";
    }
   
 
}