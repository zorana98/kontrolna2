package ac.rs.uns.ftn.projekat.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import ac.rs.uns.ftn.projekat.entity.Artikal;
import ac.rs.uns.ftn.projekat.entity.Dostavljac;
import ac.rs.uns.ftn.projekat.entity.Korisnik;
import ac.rs.uns.ftn.projekat.entity.Korpa;
import ac.rs.uns.ftn.projekat.entity.Kupac;
import ac.rs.uns.ftn.projekat.entity.Status;
import ac.rs.uns.ftn.projekat.repository.ArtikalRep;
import ac.rs.uns.ftn.projekat.repository.KorisnikRep;
import ac.rs.uns.ftn.projekat.repository.KorpaRep;
import ac.rs.uns.ftn.projekat.service.ArtikalService;
import ac.rs.uns.ftn.projekat.service.KorisnikService;
import ac.rs.uns.ftn.projekat.service.KorpaService;
import ac.rs.uns.ftn.projekat.service.KupacService;

@Controller
public class ArtikliController {
    @Autowired
    private ArtikalService artikliService;
    @Autowired
    private ArtikalRep artikliRep;
    @Autowired
    private KorisnikService korisnikService;
    @Autowired
    private KorpaService korpaService;
    @Autowired
    private KorisnikRep korisnikRepository;
    @Autowired
    private KorpaRep korpaRepository;

    @GetMapping("/kategorije/kupac")
    public String artiklii(Model model) {
        List<Artikal> artikli = this.artikliService.findAllArtikli();

        model.addAttribute("artikli", artikli);
            return "kategorijeKupac.html";
        
    
    }
    @GetMapping("/kategorije/admin")
    public String artiklii2(Model model) {
        List<Artikal> artikli = this.artikliService.findAllArtikli();
        model.addAttribute("artikli", artikli);
            return "kategorijeAdmin.html";
        
    
    }
    @GetMapping("/kategorije/dostavljac")
    public String artiklii3(Model model) {
        List<Artikal> artikli = this.artikliService.findAllArtikli();
        model.addAttribute("artikli", artikli);
            return "kategorijeDostavljac.html";
        
    
    }
    @GetMapping("/kategorije")
    public String artiklii4(Model model) {
        List<Artikal> artikli = this.artikliService.findAllArtikli();
        model.addAttribute("artikli", artikli);
            return "kategorije.html";
        
    
    }

    @GetMapping("/kategorije/{kategorija}")
    public String artikli(@Valid @ModelAttribute Artikal artikli, @PathVariable(name = "kategorija") String k,
            BindingResult errors, Model model) {
        if (k.equals("")) {
            model.addAttribute("artikli", artikliService.findAllArtikli());
        } else {
            model.addAttribute("artikli", artikliService.findByKategorijaa(k));
        }
        return "kategorije";
    }

    @GetMapping("/kategorije/{opis}/{naziv}/{od}/{do}")
    public String izbor(@Valid @ModelAttribute Artikal artikli, @PathVariable(name = "naziv") String naziv,
            @PathVariable(name = "opis") String opis, @PathVariable(name = "od") int od,
            @PathVariable(name = "do") int gornjagranica, BindingResult errors, Model model) {
        List<Artikal> lista = artikliService.findByMany(opis, naziv, od, gornjagranica);
        model.addAttribute("artikli", lista);
        return "kategorije";
    }

    @GetMapping("/kategorije/{od}/{do}")
    public String izbor2(@Valid @ModelAttribute Artikal artikli, @PathVariable(name = "od") int od,
            @PathVariable(name = "do") int gornjagranica, BindingResult errors, Model model) {
        List<Artikal> lista = artikliService.findByCena(od, gornjagranica);
        model.addAttribute("artikli", lista);
        return "kategorije";
    }

    @GetMapping("/kategorije/1/1/1/1/{naziv}")
    public String izbor3(@Valid @ModelAttribute Artikal artikli, @PathVariable(name = "naziv") String naziv,
            BindingResult errors, Model model) {
        List<Artikal> lista = artikliService.findByNazivv(naziv);
        model.addAttribute("artikli", lista);
        return "kategorije";
    }

    @GetMapping("/kategorije/sortirajn")
    public String sortiraj(@Valid @ModelAttribute Artikal artikli, BindingResult errors, Model model) {
        List<Artikal> lista = artikliService.findSortiraniNaziv();
        model.addAttribute("artikli", lista);
        return "kategorije";
    }

    @GetMapping("/kategorije/sortirajc")
    public String sortirajcena(@Valid @ModelAttribute Artikal artikli, BindingResult errors, Model model) {
        List<Artikal> lista = artikliService.findSortiraniCena();
        model.addAttribute("artikli", lista);
        return "kategorije";
    }

    @GetMapping("/kategorije/popust")
    public String popust(@Valid @ModelAttribute Artikal artikli, BindingResult errors, Model model) {
        List<Artikal> lista = artikliService.findNaPopustu();
        model.addAttribute("artikli", lista);
        return "kategorije";
    }

    @GetMapping("/korpa")
    public String korpa(@Valid @ModelAttribute Artikal artikli, BindingResult errors, Model model) {
        return "korpa";
    }

    /*
     * @PostMapping("/omiljeni/{userId}/{artikalId}") public RequestMapping
     * postOmiljeno( @PathVariable Long userId, @PathVariable Long artikalId){
     * Artikal art = this.artikliService.getOneById(artikalId); Kupac kupac =
     * (Kupac) this.korisnikService.findOne(userId); List<Artikal> list =
     * kupac.getOmiljeniArtikli(); if(!list.contains(art)) list.add(art);
     * kupac.setOmiljeniArtikli(list); return null; }
     * 
     * @GetMapping("/omiljeni/{userId}") public String omiljeni(@Valid @PathVariable
     * Long userId, Model model){ Kupac kupac = (Kupac)
     * this.korisnikService.findOne(userId); List<Artikal> list =
     * kupac.getOmiljeniArtikli(); model.addAttribute("artikli", list); return
     * "omiljeni.html"; }
     */
    @GetMapping("/omiljeni/{userId}/{artikalId}")
    public String setOmiljeno(@PathVariable("userId") long IdUsera, @PathVariable("artikalId") long IdaArtikla,
            Model model) {
        Artikal art = this.artikliService.getOneById(IdaArtikla);
        Korisnik kupac = this.korisnikService.findOne(IdUsera);
        List<Artikal> list = kupac.getOmiljeniArtikli();
        if (!list.contains(art))
            kupac.setOmiljeniArtikli(art);
        this.korisnikRepository.save(kupac);
        return "kategorije.html";
    }

    @GetMapping("/omiljeni/{userId}")
    public String getOmiljeno(@PathVariable("userId") long IdUsera, Model model) {

        Korisnik kupac = this.korisnikService.findOne(IdUsera);
        List<Artikal> list2 = kupac.getOmiljeniArtikli();
        System.out.println(list2.size());
        model.addAttribute("artikli", list2);

        return "omiljeni.html";

    }

    public HashMap<String, String> generateArtikalDAO(Artikal artikal) {
        HashMap<String, String> artikalDao = new HashMap<String, String>();

        artikalDao.put("id", artikal.getId().toString());
        artikalDao.put("naziv", artikal.getNaziv());
        artikalDao.put("cena", String.valueOf(artikal.getCena()));
        artikalDao.put("kolicina", String.valueOf(artikal.getKolicina()));
        artikalDao.put("kategorija", artikal.getKategorija());
        artikalDao.put("popust", String.valueOf(artikal.getPopust()));

        return artikalDao;

    }

    @GetMapping("/artikal/{artikalId}")
    public ResponseEntity getOneArtikal(@PathVariable Long artikalId, Model model) {
        try {
            Artikal art = this.artikliService.getOneById(artikalId);
            HashMap<String, String> artikalDao = generateArtikalDAO(art);
            return new ResponseEntity<HashMap<String, String>>(artikalDao, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    /*@PostMapping("/kupi/{userId}")
    public RequestMapping kupovina(@RequestBody HashMap<Long, Long> korpa, @PathVariable Long userId) {

        Korpa novaKorpa = new Korpa();
        for (Long artikalId : korpa.keySet()) {
            Artikal artikal = this.artikliService.getOneById(artikalId);
            Long kol = korpa.get(artikalId);
        }
        Korisnik kupac = this.korisnikService.findOne(userId);

        novaKorpa.setDatumKupovine(new Date());
        novaKorpa.setStatus(Status.Kupljeno);
        //novaKorpa.setKupac(kupac);
        // novaKorpa.setListaKupljenihArtikala(listaKupljenihArtikala);

        return null;

    }
    @GetMapping("/korpa/{userId}/{artikalId}")
    public String setKorpa(@PathVariable("userId") long IdUsera, @PathVariable("artikalId") long IdaArtikla,
            Model model) {
        Artikal art = this.artikliService.getOneById(IdaArtikla);
        Korisnik kupac = this.korisnikService.findOne(IdUsera);
        List<Artikal> list = kupac.getOmiljeniArtikli();
        if (!list.contains(art))
            kupac.setOmiljeniArtikli(art);
        this.korisnikRepository.save(kupac);
        return "kategorije.html";
    }*/
    @GetMapping("/izmeni/{id}")
    public String izmeniArtikal(@PathVariable("id") long id, Model model) {
        Artikal artikal=this.artikliService.getOneById(id);
        if(artikal.getPopust()==true){
            artikal.setPopust(false);
        }else{artikal.setPopust(true);}
        this.artikliRep.save(artikal);
        model.addAttribute("artikli", artikliService.findAllArtikli());
        return "kategorijeAdmin.html";
    }
    @GetMapping("/izmeniArtikal/{id}/{naziv}")
    public String izmeniArtikalnaziv(@PathVariable("id") long id,@PathVariable("naziv") String naziv, Model model) {
        Artikal artikal=this.artikliService.getOneById(id);
        artikal.setNaziv(naziv);
        this.artikliRep.save(artikal);
        model.addAttribute("artikli", artikliService.findAllArtikli());
        return "kategorijeAdmin.html";
    }
    @GetMapping("/izmeniArtikal/1/{id}/{kolicina}")
    public String izmeniArtikalKolicina(@PathVariable("id") long id,@PathVariable("kolicina") int kolicina, Model model) {
        Artikal artikal=this.artikliService.getOneById(id);
        artikal.setKolicina(kolicina);
        this.artikliRep.save(artikal);
        model.addAttribute("artikli", artikliService.findAllArtikli());
        return "kategorijeAdmin.html";
    }
    @GetMapping("/izmeniArtikal/1/1/{id}/{cena}")
    public String izmeniArtikalCena(@PathVariable("id") long id,@PathVariable("cena") int cena, Model model) {
        Artikal artikal=this.artikliService.getOneById(id);
        artikal.setCena(cena);
        this.artikliRep.save(artikal);
        model.addAttribute("artikli", artikliService.findAllArtikli());
        return "kategorijeAdmin.html";
    }
    @GetMapping("/izmeniArtikal/1/1/1/{id}/{kategorija}")
    public String izmeniArtikalKategorija(@PathVariable("id") long id,@PathVariable("kategorija") String kategorija, Model model) {
        Artikal artikal=this.artikliService.getOneById(id);
        artikal.setKategorija(kategorija);
        this.artikliRep.save(artikal);
        model.addAttribute("artikli", artikliService.findAllArtikli());
        return "kategorijeAdmin.html";
    }
    @GetMapping("/obrisi/{id}")
    public String obrisiArtikal(@PathVariable("id") long id, Model model) {
        Artikal artikal=this.artikliService.getOneById(id);
        
        this.artikliRep.delete(artikal);
        model.addAttribute("artikli", artikliService.findAllArtikli());
        return "kategorijeAdmin.html";
    }

    @GetMapping("/dodaj/{naziv}/{cena}/{kolicina}/{kategorija}")
    public String dodaj(@ModelAttribute Artikal artikli,@PathVariable(name = "naziv") String naziv,
            @PathVariable(name = "cena") int cena, @PathVariable(name = "kolicina") int kolicina,
            @PathVariable(name = "kategorija") String kategorija, BindingResult errors, Model model) {
        Artikal artikal=new Artikal();
        artikal.setCena(cena);
        artikal.setKategorija(kategorija);
        artikal.setKolicina(kolicina);
        artikal.setNaziv(naziv);
        this.artikliRep.save(artikal);
        List<Artikal> lista=artikliService.findAllArtikli();
        model.addAttribute("artikli", lista);
        return "kategorijeAdmin.html";
    }
    @GetMapping("/pokusajKorpa/{userId}/{artikalId}")
    public String setKorpa(@PathVariable("userId") long IdUsera, @PathVariable("artikalId") long IdaArtikla,
            Model model) {
        Artikal art = this.artikliService.getOneById(IdaArtikla);
        Korisnik kupac = this.korisnikService.findOne(IdUsera);
        Korpa k=kupac.getKorpa();
        if(art.getKolicina()>0){
            k.setLista(art);
            int kol=art.getKolicina();
            int s=kol-1;
            art.setKolicina(s);
            this.artikliRep.save(art);
        }
        else
            art.setKolicina(0);
        
          
        this.artikliRep.save(art);
        this.korpaRepository.save(kupac.getKorpa());
        this.korisnikRepository.save(kupac);
        return "kategorije.html";
    }
    @GetMapping("/pokusajKorpa/{userId}")
    public String getKorpa(@PathVariable("userId") long IdUsera, Model model) {

        Korisnik kupac = this.korisnikService.findOne(IdUsera);
        Korpa k=kupac.getKorpa();
        List<Artikal> listA=k.getLista();
        double s=0;
        for(int i=0;i<listA.size();i++){
            s=s+listA.get(i).getCena();
        }
        k.setCena(s);
        double cena=k.getCena();
        System.out.println(k.getCena());
        model.addAttribute("cena", cena);
        model.addAttribute("artikli", listA);
        this.korpaRepository.save(kupac.getKorpa());
        this.korisnikRepository.save(k.getKorisnik());
        this.korisnikRepository.save(kupac);

        return "korpa2.html";

    }
    @GetMapping("/pokusajKorpa2/{userId}")
    public String getKorpa2(@PathVariable("userId") long IdUsera, Model model) {
        Korisnik kupac = this.korisnikService.findOne(IdUsera);
        Korpa p=new Korpa();
        Date data=new Date();
        p.setDatumKupovine(data);
        p.setKorisnik(kupac);
        kupac.setKorpa(p);
        korpaRepository.save(kupac.getKorpa());
        korisnikRepository.save(kupac);
        List<Artikal> lista=artikliService.findAllArtikli();
        model.addAttribute("artikli", lista);
        return "kategorijeKupac.html";

    }
    @GetMapping("/ObrisiKorpu/{userId}")
    public String Obrisi(@PathVariable("userId") long IdUsera, Model model) {
        Korisnik kupac = this.korisnikService.findOne(IdUsera);
        Korpa k=kupac.getKorpa();
        k.setStatus(Status.Otkazano);
        this.korpaRepository.save(kupac.getKorpa());
        List<Artikal> list=k.getLista();
        for(int i=0;i<list.size();i++){
            Artikal a=list.get(i);
            int p=a.getKolicina();
            int r=p+1;
            System.out.println(r);
            a.setKolicina(r);
            System.out.println(a.getKolicina());
            this.artikliRep.save(a);

        }
        Korpa korpa=new Korpa();
        Date date = new Date();
        korpa.setDatumKupovine(date);
        /*korpa.setStatus(Status.Otkazano);*/
        korpa.setKorisnik(kupac);
        /*korpa.setDatumKupovine(date);*/
        kupac.setKorpa(korpa);
        this.korpaRepository.save(kupac.getKorpa());
        this.korisnikRepository.save(kupac);
        List<Artikal> lista=artikliService.findAllArtikli();
        model.addAttribute("artikli", lista);
        return "kategorijeKupac.html";
    }
    @GetMapping("/Kupujem/{userId}")
    public String Kupovina(@PathVariable("userId") long IdUsera, Model model) {
        Korisnik kupac = this.korisnikService.findOne(IdUsera);
        Korpa korpa=kupac.getKorpa();
        Korpa nova=new Korpa();
        Date date = new Date();
        korpa.setDatumKupovine(date);
        korpa.setStatus(Status.Kupljeno);
        List<Artikal> lista2= korpa.getLista();
        
        double s=0;
        for(int i=0;i<lista2.size();i++){
            s=s+lista2.get(i).getCena();

        }
        int novo=kupac.getPopust()+1;
        if(s>1000){kupac.setPopust(novo);}
        /*korpa.setCena(s);*/
        System.out.println(korpa.getCena());
        System.out.println(kupac.getPopust());
        korpa.setKorisnik(kupac);
        kupac.setKorpa(nova);
        kupac.setKupljeneKorpa(korpa);
        List <Korpa> korpestare=kupac.getKupljeneKorpa();
        /*System.out.println(kupac.getKupljeneKorpa());
        System.out.println(kupac.getKupljeneKorpa().size());*/
        /*for(int i=0;i<kupac.getKupljeneKorpa().size();i++){
            this.korpaRepository.save(kupac.getKupljeneKorpa().get(i));
        }*/
        this.korpaRepository.save(kupac.getKorpa());
        this.korpaRepository.save(korpa);
        this.korisnikRepository.save(korpa.getKorisnik());
        this.korisnikRepository.save(kupac);
        
        List<Artikal> lista=artikliService.findAllArtikli();
        model.addAttribute("predhodne", korpestare);
        model.addAttribute("artikli", lista);
        return "kategorijeKupac.html";
    }
    @GetMapping("/stare/{id}")
    public String StareKorpi(Model model,@PathVariable("id") long IdUsera) {
        Korisnik kupac = this.korisnikService.findOne(IdUsera);
        List<Korpa> stare=kupac.getKupljeneKorpa();
        List<Artikal> artikli_kupljeni= new ArrayList<Artikal>();
        for(int i=0;i<stare.size();i++){
            for(int j=0;j<stare.get(i).getLista().size();j++){
                Artikal trenutni=stare.get(i).getLista().get(j);
                artikli_kupljeni.add(trenutni);
            }
        }
        model.addAttribute("artikli",artikli_kupljeni);
        model.addAttribute("predhodne",stare);
        return "korpestare.html";
    }
    
    @GetMapping("/korpe")
    public String PrikazKorpi(Model model) {
        List<Korpa> k= this.korpaService.findAllKupljene();
        model.addAttribute("korpesve", k);
        return "korpe.html";
    }
    @GetMapping("/Dostavljam/{userId}/{idD}")
    public String Kupovina(@PathVariable("userId") long IdUsera,@PathVariable("idD") long IdDo, Model model) {
        
        Korpa trazena=this.korpaService.findTrazenu(IdUsera);
        System.out.println(trazena.getKorisnik());
        trazena.setStatus(Status.Dostava);
        this.korpaRepository.save(trazena);
        model.addAttribute("korpamoja", trazena);
        return "korpecekamdadostavim.html";
    }
    @GetMapping("/ZavrsioDostavu/{userId}/{idD}")
    public String Dostavljeno(@PathVariable("userId") long IdUsera,@PathVariable("idD") long IdDo, Model model) {
        
        Korpa trazena2=this.korpaService.findTrazena2(IdUsera);
        trazena2.setStatus(Status.Dostavljeno);
        this.korpaRepository.save(trazena2);
        List<Korpa> k=this.korpaService.findAllKupljene();
        model.addAttribute("korpesve", k);
        return "korpe.html";
    }
    @GetMapping("/Popust10/{userId}")
    public String Popust10(@PathVariable("userId") long IdUsera, Model model) {
        Korisnik kupac = this.korisnikService.findOne(IdUsera);
        Korpa korpa=kupac.getKorpa();
        if(kupac.getPopust()>=5){
            double stara_cena=korpa.getCena();
            double nova_cena=stara_cena*0.9;
            korpa.setCena(nova_cena);
            int stari_kuponi=kupac.getPopust();
            int novi_kuponi=stari_kuponi-5;
            kupac.setPopust(novi_kuponi);
        }
        this.korpaRepository.save(kupac.getKorpa());
        this.korisnikRepository.save(korpa.getKorisnik());
        this.korisnikRepository.save(kupac);
        List<Artikal> list2=korpa.getLista();
        double cena=korpa.getCena();
        model.addAttribute("cena", cena);
        model.addAttribute("artikli", list2);
        return "korpa2.html";
    }
    @GetMapping("/Popust20/{userId}")
    public String Popust20(@PathVariable("userId") long IdUsera, Model model) {
        Korisnik kupac = this.korisnikService.findOne(IdUsera);
        Korpa korpa=kupac.getKorpa();
        if(kupac.getPopust()>=10){
            double stara_cena=korpa.getCena();
            double nova_cena=stara_cena*0.8;
            korpa.setCena(nova_cena);
            int stari_kuponi=kupac.getPopust();
            int novi_kuponi=stari_kuponi-10;
            kupac.setPopust(novi_kuponi);
        }
        this.korpaRepository.save(kupac.getKorpa());
        this.korisnikRepository.save(korpa.getKorisnik());
        this.korisnikRepository.save(kupac);
        List<Artikal> list2=korpa.getLista();
        double cena=korpa.getCena();
        model.addAttribute("cena", cena);
        model.addAttribute("artikli", list2);
        return "korpa2.html";
    }
    @GetMapping("/izvestaj")
    public String Izvestaj(Model model) {
        List<Korpa> korpe=this.korpaService.findAllDostavljene();
        double zarada=0;
        for(int i=0;i<korpe.size();i++){
            double p=korpe.get(i).getCena();
            zarada=zarada+p;
        }
        double otkazane=0;
        List<Korpa> korpe2=this.korpaService.findAllOtkazane();
        System.out.println(korpe2.size());
        otkazane=otkazane+korpe2.size();
        model.addAttribute("zarada", zarada);
        model.addAttribute("otkazane", otkazane);
        model.addAttribute("korpesve", korpe);
        return "izvestaj.html";
    }




}