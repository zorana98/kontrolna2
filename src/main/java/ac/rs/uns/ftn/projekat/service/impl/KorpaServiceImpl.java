package ac.rs.uns.ftn.projekat.service.impl;

import ac.rs.uns.ftn.projekat.entity.Artikal;
import ac.rs.uns.ftn.projekat.entity.Korisnik;
import ac.rs.uns.ftn.projekat.entity.Korpa;
import ac.rs.uns.ftn.projekat.entity.Kupac;
import ac.rs.uns.ftn.projekat.entity.Status;
import ac.rs.uns.ftn.projekat.entity.Uloga;
import ac.rs.uns.ftn.projekat.repository.ArtikalRep;
import ac.rs.uns.ftn.projekat.repository.KorpaRep;
import ac.rs.uns.ftn.projekat.repository.KupacRep;
import ac.rs.uns.ftn.projekat.service.KorisnikService;
import ac.rs.uns.ftn.projekat.service.KorpaService;
import ac.rs.uns.ftn.projekat.service.KupacService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KorpaServiceImpl implements KorpaService {

    @Autowired
    public KorpaRep korpaRep;
    
    @Override
    public List<Korpa> findAllKupljene(){
        List<Korpa> korpe=korpaRep.findAllByStatus(Status.Kupljeno);
        return korpe;
    }
    @Override
    public Korpa findTrazenu(Long id){
        Korpa korpa=korpaRep.findByIdAndStatus(id,Status.Kupljeno);
        return korpa;
    }
    @Override
    public Korpa findTrazena2(Long id){
        Korpa korpa=korpaRep.findByIdAndStatus(id,Status.Dostava);
        return korpa;
    }
    @Override
    public List<Korpa> findAllDostavljene(){
        List<Korpa> korpe=korpaRep.findAllByStatus(Status.Dostavljeno);
        return korpe;
    }
    @Override
    public List<Korpa> findAllOtkazane(){
        List<Korpa> korpe=korpaRep.findAllByStatus(Status.Otkazano);
        return korpe;
    }
    
}  