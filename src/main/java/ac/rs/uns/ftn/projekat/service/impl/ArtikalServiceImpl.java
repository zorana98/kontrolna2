package ac.rs.uns.ftn.projekat.service.impl;


import ac.rs.uns.ftn.projekat.entity.Artikal;
import ac.rs.uns.ftn.projekat.repository.ArtikalRep;
import ac.rs.uns.ftn.projekat.service.ArtikalService;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class ArtikalServiceImpl implements ArtikalService {

    @Autowired
    private ArtikalRep artikalRep;
    /*@Autowired
    public List<Artikal> findByKategorijaa(String kategorija){
        List<Artikal> artikli = ArtikalRep.findByKategorija(kategorija);
        return artikli;
    }*/
    @Override
    public List<Artikal> findAllArtikli(){

        List<Artikal>  artikli=artikalRep.findAll();
        return artikli;
    }
    @Override
    public List<Artikal> findByKategorijaa(String kategorija){

        List<Artikal>  artikli=artikalRep.findByKategorija(kategorija);
        return artikli;
    }
    @Override
    public List<Artikal> findByMany(String opis,String naziv,int od,int gornjagranica){
        List<Artikal> artikli = artikalRep.findAll();
        List<Artikal> artikliizabrani = new ArrayList<Artikal>();
        for (Artikal a : artikli) {
                if(a.getNaziv().toUpperCase().contains(naziv.toUpperCase())
                && a.getCena()<gornjagranica
                && a.getCena()>od
                )
                    {artikliizabrani.add(a);}
        }
        return artikliizabrani;
    }
    @Override
    public List<Artikal> findByCena(int od,int gornjagranica){
        List<Artikal> artikal = artikalRep.findAll();
        List<Artikal> artikli = new ArrayList<Artikal>();
        for (Artikal a : artikal) {
                if(a.getCena()<gornjagranica
                && a.getCena()>od
                )
                    {artikli.add(a);}
        }
        return artikli;
    }
    @Override
    public List<Artikal> findByNazivv(String naziv){
        List<Artikal> artikal = artikalRep.findAll();
        List<Artikal> artikliizabrani = new ArrayList<Artikal>();
        for (Artikal a : artikal) {
                if(a.getNaziv().toUpperCase().contains(naziv.toUpperCase())

                )
                    {artikliizabrani.add(a);}
        }
      
        return artikliizabrani;
    }
    @Override
    public List<Artikal> findSortiraniNaziv(){
        List<Artikal> artikal=artikalRep.findAll(Sort.by("naziv"));

       
        return artikal;
    }
    @Override
    public List<Artikal> findSortiraniCena(){
        List<Artikal> artikal=artikalRep.findAll(Sort.by("cena"));
      
        return artikal;
    }
    @Override
    public List<Artikal> findNaPopustu(){
    List<Artikal> artikal=artikalRep.findAll();
    List<Artikal> artiklinovi=new ArrayList<Artikal>();
         for (Artikal a : artikal) {
                if(a.getPopust()) {
                    artiklinovi.add(a);
                }
        }
       return artiklinovi;


    }
    @Override
    public Artikal getOneById(Long id){
        Artikal art = this.artikalRep.getOne(id);
        return art;
    }




}