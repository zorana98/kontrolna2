package ac.rs.uns.ftn.projekat.repository;

import ac.rs.uns.ftn.projekat.entity.Artikal;
import ac.rs.uns.ftn.projekat.entity.Kupac;

import org.springframework.data.jpa.repository.JpaRepository;

public interface KupacRep extends JpaRepository<Kupac,Long> {
    /*Kupac findOne(Long Id);*/
}
