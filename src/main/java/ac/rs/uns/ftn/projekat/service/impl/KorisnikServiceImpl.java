package ac.rs.uns.ftn.projekat.service.impl;


import ac.rs.uns.ftn.projekat.entity.Artikal;
import ac.rs.uns.ftn.projekat.entity.Korisnik;
import ac.rs.uns.ftn.projekat.entity.Uloga;
import ac.rs.uns.ftn.projekat.repository.ArtikalRep;
import ac.rs.uns.ftn.projekat.repository.KorisnikRep;
import ac.rs.uns.ftn.projekat.service.KorisnikService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KorisnikServiceImpl implements KorisnikService {

    @Autowired
    public KorisnikRep korisnikRep;
    @Autowired
    public ArtikalRep artikalRep;

    @Override
    public Korisnik registracija(Korisnik korisnik) throws Exception {
        this.korisnikRep.save(korisnik);
        return korisnik;
    }
    @Override
    public Korisnik create(Korisnik korisnik) throws Exception {

        if (korisnik.getId() != null)
            throw new Exception("ID must be null!");
        
        this.korisnikRep.save(korisnik);
        return korisnik;
    
    }
    @Override
    public List<Korisnik> findName(String imeKorisnika)
    {
        System.out.println(imeKorisnika);
        List<Korisnik> korisnici = korisnikRep.findAllByKorisnickoime(imeKorisnika);
        return korisnici;
    }
    @Override
    public void change(Long id){
        Korisnik korisnik= korisnikRep.getOne(id);
        /*korisnikRep.delete(korisnik);*/
        if(korisnik.getUloga().equals(Uloga.ADMINISTRATOR))
        korisnik.setUloga(Uloga.KUPAC);
        else if(korisnik.getUloga().equals(Uloga.KUPAC))
        korisnik.setUloga(Uloga.ADMINISTRATOR);
        /*korisnikRep.save(korisnik);*/
    }

    @Override
    public void delete(Long id){
        Korisnik korisnik=korisnikRep.getOne(id);
        korisnikRep.delete(korisnik);

    }
    @Override
    public Korisnik findNamePassword(String imeKorisnika, String Lozinka)
    {
        System.out.println(imeKorisnika+Lozinka);
        Korisnik korisnici = korisnikRep.findByKorisnickoimeAndLozinka(imeKorisnika, Lozinka);
        return korisnici;
    }

    @Override
    public Korisnik findOneForRegister(String ime) {
        Korisnik korisnik = this.korisnikRep.findByIme(ime);
        return korisnik;
    }

    @Override
    public Korisnik login(Korisnik korisnik) throws Exception {
        if (korisnik.getKorisnickoime() == null || korisnik.getLozinka() == null) {
            throw new Exception("Neispravni podaci!!");
        }
        Korisnik newKorisnik = this.korisnikRep.findByKorisnickoime(korisnik.getKorisnickoime());
        return newKorisnik;
    }


    @Override
    public List<Korisnik> findAll() {
        List<Korisnik> korisnici =  this.korisnikRep.findAll();
        return korisnici;
    }

    @Override
    public Korisnik findOne(Long id){
        Korisnik korisnik=this.korisnikRep.getOne(id);
        return korisnik;
    }
    @Override
    public void setOmiljeni(Long IdUsera,Long IdaArtikla){
        Artikal a = artikalRep.getOne(IdaArtikla);
        Korisnik k = korisnikRep.getOne(IdUsera);
        k.setOmiljeniArtikli(a);
        korisnikRep.saveAndFlush(k);


    }
 

}
