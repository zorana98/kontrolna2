package ac.rs.uns.ftn.projekat.entity;

public enum Status {
    Kupljeno, Dostava, Otkazano, Dostavljeno
}