package ac.rs.uns.ftn.projekat.service;

import java.util.List;

//import ac.rs.uns.ftn.projekat.repository.KorisnikRep;

//import ac.rs.uns.ftn.projekat.entity.Uloga;

import org.springframework.stereotype.Service;

import ac.rs.uns.ftn.projekat.entity.Artikal;
import ac.rs.uns.ftn.projekat.entity.Korpa;

@Service
public interface KorpaService {

List<Korpa> findAllKupljene();

Korpa findTrazenu(Long id);
Korpa findTrazena2(Long id);

List<Korpa> findAllDostavljene();

List<Korpa> findAllOtkazane();

}