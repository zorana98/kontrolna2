package ac.rs.uns.ftn.projekat.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Artikal implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name="naziv")
    private String naziv;
    @Column(name="opisartikla")
    private String opisArtikla;
    @Column(name="cena")
    private double cena;
    @Column(name="kolicina")
    private int kolicina;
    @Column(name="kategorija")
    private String kategorija;
    @Column(name="popust")
    private Boolean popust;
    
    @ManyToMany(mappedBy = "omiljeniArtikli")
    private Set<Kupac> omiljeniKupci = new HashSet<>();

    
    @ManyToOne
    private Korpa korpaArtikli;

    public String getNaziv() {
        return naziv;
    }

    public String getOpisArtikla() {
        return opisArtikla;
    }

    public double getCena() {
        return cena;
    }

    public int getKolicina() {
        return kolicina;
    }

    public String getKategorija() {
        return kategorija;
    }

    /*public Korisnik getKorisnik() {
        return korisnik;
    }*/

    public Long getId() {
        return id;
    }

    public Korpa getKorpaArtikli() {
        return korpaArtikli;
    }
    public boolean getPopust(){
        return popust;
    }
    public void setPopust(Boolean popust){
        this.popust=popust;
    }
    
    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public void setOpisArtikla(String opisArtikla) {
        this.opisArtikla = opisArtikla;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    /*public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }*/

    public void setId(Long id) {
        this.id = id;
    }

    public void setKorpaArtikli(Korpa korpaArtikli) {
        this.korpaArtikli = korpaArtikli;
    }
    

    @Override
    public String toString() {
        return "ArtikalRepository{" +
                "naziv='" + naziv + '\'' +
                ", opisArtikla='" + opisArtikla + '\'' +
                ", cena=" + cena +
                ", kolicina=" + kolicina +
                ", kategorija='" + kategorija+
                '}';
    }
}
