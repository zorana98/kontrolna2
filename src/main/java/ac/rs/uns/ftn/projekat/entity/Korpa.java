package ac.rs.uns.ftn.projekat.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;




@Entity
public class Korpa implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="Datum_porudzbine")
    private Date datumKupovine;
    @Column(name="Status")
    private Status status;
    @Column(name="cena")
    private double cena;
    /*@OneToOne()
    private Korisnik korisnik;*/
    /*@OneToOne()
    private Dostavljac dostavljac;*/

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Dostavljac dostavljaci;

    /*@OneToMany(mappedBy = "korpa", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    List<Artikal> listaKupljenihArtikala = new ArrayList<Artikal>();*/

    @OneToOne(mappedBy = "korpaKupac", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
    private Korisnik korisnik;

    /*@ManyToOne
    private Dostavljac dostavljac;*/


    @ManyToMany
    @JoinTable(name = "listaArtikla", joinColumns = @JoinColumn(name = "korisnik_id",referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "artikal_id",referencedColumnName = "id"))
    private List<Artikal> lista = new ArrayList<Artikal>();

    public List<Artikal> getLista() {
        return lista;
    }

    public void setLista(Artikal oA) {
        this.lista.add(oA);
    }
    public void setCena(double cena) {
        this.cena=cena;
    }
    public Long getId() {
        return id;
    }

    public double getCena() {
        return cena;
    }

    public Date getDatumKupovine() {
        return datumKupovine;
    }

    public Status getStatus() {
        return status;
    }
    
    public Korisnik getKorisnik() {
        return korisnik;
    }

    

    public Dostavljac getDostavljaci() {
        return dostavljaci;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLista(List<Artikal> lista) {
        this.lista = lista;
    }

    public void setDatumKupovine(Date datumKupovine) {
        this.datumKupovine = datumKupovine;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public void setDostavljac(Dostavljac dostavljaci) {
        this.dostavljaci = dostavljaci;
    }

    

    @Override
    public String toString() {
        return "KorpaRepository{" +
                "id=" + id +
                ", listaKupljenihArtikala=" + lista +
                ", datumKupovine=" + datumKupovine +
                ", status=" + status +
                ", kupac=" + korisnik +
                ", dostavljac=" + dostavljaci+
                '}';
    }
}
