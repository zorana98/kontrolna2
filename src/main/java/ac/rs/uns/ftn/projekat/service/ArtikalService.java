package ac.rs.uns.ftn.projekat.service;


import java.util.List;

//import ac.rs.uns.ftn.projekat.repository.KorisnikRep;
//import ac.rs.uns.ftn.projekat.entity.Uloga;



import org.springframework.stereotype.Service;

import ac.rs.uns.ftn.projekat.entity.Artikal;

@Service
public interface ArtikalService {

List<Artikal> findByKategorijaa(String kategorija);
List<Artikal> findAllArtikli();
List<Artikal> findByCena(int od,int gornjagranica);
List<Artikal> findByMany(String naziv, String opis,int od, int gornjagranica);
List<Artikal> findByNazivv(String naziv);
List<Artikal> findSortiraniNaziv();
List<Artikal> findSortiraniCena();
List<Artikal> findNaPopustu();


Artikal getOneById(Long id);
}