package ac.rs.uns.ftn.projekat.repository;

import ac.rs.uns.ftn.projekat.entity.Korisnik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface KorisnikRep extends JpaRepository<Korisnik,Long> {
    Korisnik findByIme(String ime);
    Korisnik findByKorisnickoime(String imeKorisnika);
    Korisnik findByKorisnickoimeAndLozinka(String korIme, String Lozinka);
    List<Korisnik> findAll();
    Optional<Korisnik> findById(Long id);
    List<Korisnik> findAllByKorisnickoime(String korisnickoime);
    
}

