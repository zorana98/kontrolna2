package ac.rs.uns.ftn.projekat;

import ac.rs.uns.ftn.projekat.repository.ArtikalRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjekatApplication implements CommandLineRunner {
	@Autowired
	private   ArtikalRep artikalRep;

	@Override
	public void run(String... args) {
	}

	public ArtikalRep getArtikalRep() {
		return artikalRep;
	}

	public void setArtikalRep(ArtikalRep artikalRep) {
		this.artikalRep = artikalRep;
	}

	public static void main(String[] args) {
		SpringApplication.run(ProjekatApplication.class, args);
	}

}
