package ac.rs.uns.ftn.projekat.service;


import ac.rs.uns.ftn.projekat.entity.Korisnik;
//import ac.rs.uns.ftn.projekat.repository.KorisnikRep;
//import ac.rs.uns.ftn.projekat.entity.Uloga;

import java.util.List;



//import org.hibernate.bytecode.enhance.spi.UnloadedField;

//import javax.validation.Valid;


import org.springframework.stereotype.Service;

@Service
public interface KorisnikService {
   
    Korisnik create(Korisnik korisnik) throws Exception;
    Korisnik registracija(Korisnik korisnik) throws Exception;
    Korisnik findOneForRegister(String ime);
    Korisnik login(Korisnik korisnik) throws Exception;
    void change(Long id);

    //Korisnik findOne(String korisnickoime);
    Korisnik findOne(Long id);
    List<Korisnik> findAll();
    List<Korisnik> findName(String imeKorisnika);
	
	Korisnik findNamePassword(String korisnickoime, String lozinka);
    void delete(Long id);
    void setOmiljeni(Long IdUsera,Long IdaArtikla);
	


	

}
